$(document).ready(function(){
	$("#btnnya-estimasi").hide();
	$("#pemberitahuannya").hide();

	setTimeout(function mulai() {
		$("#btnnya-estimasi").show(250);
		$("#pemberitahuannya").show(1100);
	}, 6000);

	$(".ket-proyek").click(function(){
		$(".ket-proyek").toggle(100);
	});

	arrow();
	keluar();
	
});

function arrow() {
	var kedipan = 500; 
	var dumet = setInterval(function () {
	    var ele = document.getElementById('arrow');
	    ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden');
	    // $(".gbr-arrow").toggle(500);
	}, kedipan);
}

function keluar() {
	$(".btn-arrow").click(function(){
		$("#btnnya-estimasi").show(250);
		$("#pemberitahuannya").show(1100);
	});
}

$('#btn-estimasi').click(function () {
    if ($("input[name=persetujuan1]").prop('checked') == false || $("input[name=persetujuan2]").prop(
                    'checked') == false) {
        swal({
            icon: 'error',
            title: 'Error',
            text: 'Klik Persetujuan Terlebih Dahulu',
        });
        return false;
    }
});

$(document).on('click', '.tipeproject', function () {
    if ($(this).prop('checked') == true) {
        $(this).prev().addClass('active');
        $(this).prev().find("i").css('display', 'block');
    } else {
        $(this).prev().removeClass('active');
        $(this).prev().find("i").css('display', 'none');
    }
});

$('.lingkup-item').on('click', function () {
    $('.lingkup-item').removeClass('active');
    $('.lingkup-item > i').css('display', 'none');
    $(this).addClass('active');
    $(this).find("i").css('display', 'block');
});

var has_error = false;
if (has_error) {
    swal({
        icon: 'error',
        title: 'Error',
        text: 'Masukkan Data Dengan Benar',
    });
}
